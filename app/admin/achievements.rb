ActiveAdmin.register Achievement do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :hashed_id, :name, :points, :icon, :distribution_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:hashed_id, :name, :points, :icon, :distribution_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  index do
    selectable_column
    id_column
    column :icon do |i|
      image_tag(i.icon.url(:standard))
    end
    column :name
    column :points
    column :distribution.name
    actions
  end

  filter :name
  filter :points

  show do
    attributes_table do
      row :hashed_id
      row :name
      row :points
      row :distribution.name
      row :icon do |i|
        image_tag(i.icon.url(:standard))
      end
    end
  end
  
end
