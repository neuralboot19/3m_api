ActiveAdmin.register AssignmentAchieved do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :hashed_id, :user_id, :achievement_id, :distribution_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:hashed_id, :user_id, :achievement_id, :distribution_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
