ActiveAdmin.register Customer do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :email, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :sign_in_count, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :deleted_at, :uuid, :first_name, :last_name, :type, :birthday, :provider, :avatar, :mobile_token, :mobile_token_expiration, :hashed_id, :mobile_push_token, :uid, :access_token, :remote_avatar, :status, :level, :points, :distribution_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:email, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :sign_in_count, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :deleted_at, :uuid, :first_name, :last_name, :type, :birthday, :provider, :avatar, :mobile_token, :mobile_token_expiration, :hashed_id, :mobile_push_token, :uid, :access_token, :remote_avatar, :status, :level, :points, :distribution_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
