ActiveAdmin.register Game do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :hashed_id, :title, :img, :show, :distribution_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:hashed_id, :title, :img, :show, :distribution_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  index do
    selectable_column
    id_column
    column :title
    column :img do |i|
      image_tag(i.img.url(:standard))
    end
    column :distribution.name
    column :show
    actions
  end

  filter :title
  filter :show

  show do
    attributes_table do
      row :hashed_id
      row :title
      row :img do |i|
        image_tag(i.img.url(:standard))
      end
      row :distribution.name
      row :show
    end
  end
  
end
