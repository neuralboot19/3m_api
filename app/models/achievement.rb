class Achievement < ApplicationRecord
  include Hashable

  has_many :assignment_achieveds
  belongs_to :distribution

  mount_uploader :icon, ImgUploader
end
