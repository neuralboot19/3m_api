class Distribution < ApplicationRecord
  has_many :users, dependent: :destroy
  has_many :achievements, dependent: :destroy
  has_many :assignment_achieveds, dependent: :destroy
  has_many :games, dependent: :destroy
  has_many :levels, dependent: :destroy
  has_many :operations, dependent: :destroy
end
