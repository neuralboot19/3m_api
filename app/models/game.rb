class Game < ApplicationRecord
  include Hashable
  belongs_to :distribution
  mount_uploader :img, ImgUploader
end
