class AddDistributionsToMultiModel < ActiveRecord::Migration[7.0]
  def change
    add_reference :users, :distribution, null: false, foreign_key: true
    add_reference :achievements, :distribution, null: false, foreign_key: true
    add_reference :assignment_achieveds, :distribution, null: false, foreign_key: true
    add_reference :games, :distribution, null: false, foreign_key: true
    add_reference :levels, :distribution, null: false, foreign_key: true
    add_reference :operations, :distribution, null: false, foreign_key: true
  end
end
