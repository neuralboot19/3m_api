class AddTwoResultToOperation < ActiveRecord::Migration[7.0]
  def change
    add_column :operations, :two_result, :string
  end
end
