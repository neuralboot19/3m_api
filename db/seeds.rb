# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Distribution.create!(name:'system') if Rails.env.development?
Distribution.create!(name:'1mg') if Rails.env.development?
Distribution.create!(name:'2mg') if Rails.env.development?
Administrator.create!(email: 'test@test.com', password: '123456', password_confirmation: '123456', first_name: 'Admin', last_name: 'Admin', distribution_id: 1) if Rails.env.development?
